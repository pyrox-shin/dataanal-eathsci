---
title: 10/17 - Digital Signal Processing
author: Ui-Ho, Ang
Date: Oct., 17, 2022

---

# 10/17 - Digital Processing

## Error and uncertainty

- 誤差（error）：量到的數值與真實數值的差值
- 不確定性（uncertainty）：測量上造成的誤差，通常是一個range

因為常常不確定可以計算的真實值

Probability-density plot：機率密度分布圖

$\displaystyle x=x_{best} \pm \delta x$ˊ

誤差的重點是該delta對於xb的大小(b for best)

### Propagating of errors（誤差傳遞）

$x \pm \delta x$和$y\pm\delta y$之間的相加、相減

最大值之s為$s\approx (x+y)\pm(\delta x+ \delta y)$

最小值之s為$s\approx (x-y)\pm(\delta x+ \delta y)$

若$p=xy, q=\frac{x}{y}$，則其誤差為

$\displaystyle
\frac{\delta q}{\lvert q_\mathrm{best} \rvert} = \frac{\delta p}{\lvert p_{\mathrm{ best}}\rvert} 
= \sqrt{\left(\frac{\delta x}{\lvert x_{\mathrm{best}}\rvert}\right)^2 + \left(\frac{\delta y}{\lvert y_{\mathrm{best}}\rvert}\right)^2}$

若$t=nx$，則$\delta t=n \delta x$

若$r=x^n$，則$\delta r =\displaystyle n\frac{\delta x}{\lvert x_{best}\rvert}\lvert r_{best}\rvert$

(section-label)=
## Python

基本運算語法：

```python
11//3 #整除
round(11/3) #四捨五入到整數
11%3 #計算餘數

x=12
y='Hello World'
type(12) #知道變數的狀態，可能是int(整數)、float(小數)、str(字串)
newx=j #j代表i(複數)
lens(x) #文字長度
5**3 #次方

# try s.[tab] 可以在s之下尋找要執行的程式

s.replace("world", "earth") #將s中的world設為earth
```

設定路徑：

```python
usrdir = "/Users/pattylin"
sourcedir = usrdir+"/PGS/"
print(sourcedir)
```

numpy模組：數學運算模組

```python
import numpy as np
print(dir(np)) #顯示np裡面的所有指令
np.pi #從np中叫出pi
np.sqrt(3) #使用np中的sqrt計算3
np.exp(0) #計算指數函數0次方
np.sin(90*(np.pi/180)) #計算弧度轉換角度
```

```python
dl = 0.1/100       # convert unit from cm to m
lbest = 92.95/100  # convert unit from cm to m
dT = 0.004
Tbest = 1.936

gbest=(4*((np.pi)**2)*lbest)/(Tbest**2)
dG=np.sqrt((dl/lbest)**2+(2*dT/Tbest)**2)
gbr=round(gbest, 5)
dGr=round(gbr*dG, 3)
print(gbr,'ms^-2,',dGr)
```

跑出`9.79035 ms^-2, 0.042`

```python
mylist = [3,6,1,7, 19, 2,4]
del mylist #刪除該list
mylist[0] #叫出地0個數字，會出現3
```

