# Assignment #2

## Problem #1

For given magnetic susceptibility:

- mineral: $\mu_{0}=700\pm 80(tesla)$
- rock: $\mu_{n}=200 \pm70 (tesla)$

find $\mu_{0-n}$ and its uncertainty.

### Solution:

${\mu_{0-n}}_{best}=700-200=500(tesla)$

uncertainty: $\delta \mu_{0-n}=\sqrt{80^2+70^2}=10\sqrt{113}\approx106.63$

- $\mu_{0-n}=500\pm 106.63(tesla)$

## Problem #2

Dupuit-Forchheimer discharge formula:

$\displaystyle Q=\frac{k\rho gw}{2\mu t}({h_0}^2-{h_1}^2)$

For given:

- $k=(1.0 \pm 0.05)\times 10^{-11}m^2$
- ${}
\rho=1g/cm^3=10^{6}g/m^3$
- $\mu=(1.0\pm 0.02)\times 10^{-3}Pa \cdot s=(1.0\pm 0.02)g\cdot s\cdot m^{-1}$
- $t=125\pm5m$
- $w=100\pm 2m$
- $h_0=60\pm 3m$
- $h_1=20\pm3m$
- $g=10m\cdot s^{-2}$

determine Q and its uncertainty.

### Solution:

Q的單位：$\displaystyle \frac{m^2\cdot {g}\cdot{m^{-3}}\cdot m\cdot s^{-2}\cdot m}{g\cdot m^{-1}\cdot m}\times m^2=m^3/s=10^3l/s$

$\begin{aligned}\displaystyle Q&=\frac{10^{-11}\times10^{6}\times10\times100}{2\times1\times125}(60^2-20^2)
\\&=1.28\times10^{-3}(m^3/s)
\\&=128(l/s)\end{aligned}$

$\displaystyle 
\begin{aligned}
{\delta Q}{}
&=\sqrt{{(\frac{\delta k}{k_{best}}})^2+{(\frac{\delta w}{w_{best}}})^2+{(\frac{\delta \mu}{\mu_{best}}})^2+{(\frac{\delta t}{t_{best}}})^2+({\frac{\sqrt{(2\times\frac{\delta h_0}{{h_0}_{best}}\times {h_0}^2)^2+({2\times\frac{\delta h_1}{{h_1}_{best}}\times{h_1}^2)^2}}}{{{h_0}_{best}}^2-{{h_1}_{best}}^2}})^2} \times Q_{best}\\
&=\sqrt{{(\frac{5\times10^{-13}}{10^{-11}}})^2+{(\frac{2}{100}})^2+{(\frac{0.02}{1}})^2+{(\frac{5}{125}})^2+(\frac{\sqrt{(2\times\frac{3}{60}\times3600)^2+({2\times\frac{3}{20}\times400)^2}}}{60^2-20^2})^2} \times1.28\times10^{-3}
\\&=0.137704\times128\times10^{-3}\\
&=0.01762(m^3/s)\\
&=17.626(l/s) 
\end{aligned}$

- $Q=128\pm 17.626(l/s)$

（因為想要練習打$\LaTeX$所以用了notion的方式匯出）

## I

$\displaystyle
\frac{0.106}{204.22}\times1=C_{M1}\times4.30\times1 \\$

$C_{M1}=1.207\times10^{-4}(M)$

$\displaystyle
\frac{0.119}{204.22}\times1=C_{M2}\times6.76\times1 \\$

$C_{M1}=8.620\times10^{-5}(M)$

$\displaystyle
C_M=\frac{C_{M1}+C_{M2}}{2}=1.034\times10^{-4}$

## II

$\displaystyle
C_M\times20\times1=(1.034\times10^{-4})\times(\frac{14.95+21.21}{2})\times1$

$\displaystyle
C_M=8.538\times10^{-5}$
