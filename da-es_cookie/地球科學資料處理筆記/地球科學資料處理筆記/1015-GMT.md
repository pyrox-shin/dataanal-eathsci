---
title: GMT製圖
author: Ui-Ho, Ang
Date: Otc. 15, 2021

---

# 10/15 - GMT製圖

## 緒論

- 向量圖：數學公式形成的數位圖形
- 點陣圖：用像素表示

python的地圖很醜是因為他用MATLAB的圖，然後MATLAB的圖本來就很醜

所以要用GMT(好看ㄉ地圖專家)

Windows: 用terminal使用

## 產圖的範圍

```shell
gmt begin NorthTW_mapview pdf #建立檔案與副檔名

	gmt basemap -JM13c- -R121.3/121.8/25.0/25.4 -Ba0.1f0.1g0.1 -V

gmt end (show)

pause #顯示步驟
```
`-J`  : 投影（後面的13c代表13公分）

- `-JM`  : 麥卡托
- `-JA` : 蘭伯特
- `-JB` : 圓錐投影
- [其他可以看這裡](https://docs.generic-mapping-tools.org/6.4/cookbook/options.html#projections-specifications)


`-R` : 經度緯度的範圍

`-B` : 單位標記

`-V` : Verbose feedback（讓你知道自己在做甚麼）

```Shell
gmt basemap -JM13c -R121.3/121.8/25.0/25.4 -Ba0.1f0.1g0.1 -B(WESN)+t"Bouguer Anomoly Map" -V
```
`-B+t”___”`的方式可以新增地圖的標題

WESN代表經緯度顯示方位，小寫可以使其消失

## 畫海岸線

```shell
gmt coast -Df -W3 -SLightblue -G0/150/0
```

`-D` : 海岸線的線條

`- f`,`-h`、`-l` 代表圓滑化程度的控制

`-W` : 線條粗度與顏色

`-S` : Sea(海洋的顏色)

`-G` : Ground(陸地的顏色)

## 畫測站分布

```shell
gmt plot CWB_site_20221012_GMTinput.d -St0.3c -Gblue
```

`-S`: 標記種類
- 0.3c代表大小，單位為公分
- t代表形狀
- G: 標記色彩

![](1015-GMT/GMT_base_symbols1.png)

一些關於形狀的代號，如果想要找到更多資訊，可以到[GMT的官方指示文件](https://docs.generic-mapping-tools.org/6.4/plot.html#s)中尋找

## 新增文字

```shell
gmt text text1_input1015.txt -F+f+a+j
```

後面的F+f+a+j代表各項的意思

- F : XY座標
- f : font
- a : angle
- j : justify(錨點) CM代表Center, Bottom

```shell
gmt text text2_input1015.txt -F+a+jCM+f25P,Times-Bold,black -Ggreen -W2p -N -C+tO
```
可以在文字後面直接逗號加上”自型-粗細”、自行顏色

![](1015-GMT/GMT_base_text.png)

`-G` : 填滿顏色

`-W` : 邊框粗度

`-N` : 忽略圖形邊框範圍

`-C+O` : 讓文字圓框角

- 也可以用`-W！`

### 小技巧

```shell
gmt begin MyFigure pdf,jpg A+m0.5c -C
```

可以裁切乾淨！`A+m0.5c`是增加0.5公分的邊框，

`-C` : 重新乾淨新的設定

```shell
gawf -F, "{print $4,$5}" CWB_site_20221012.csv>CWB_site_20221012_forGMT.txt
```

幫我extract該csv的第四行和第五行的資料的有趣小資料,並且幫我輸出成txt

## 製作色階檔

```shell
gmt makecpt -Crainbow -T-30/30/10 -Z
gmt plot gravity.xyg -St0.3c -C
```

`makecpt`指令: 製作色階檔

- `-C `: 母色階的檔名 [色票參考](https://docs.generic-mapping-tools.org/6.4/cookbook/cpts.html?highlight=cpts)
- `-T `: 上下屆與間隔
- `-Z `: 連續色階變化

如果要讓程式幫忙自己抓可以打

```shell
gmt makecpt -Crainbow gravity.xyg -E24
```

## 繪製色階表

```python
gmt colorbar -Dx6.5c/-2c+w13c/0.5c+h+jTC+ml -Bxa10f+l"Bouguer Anomaly" -Bylmgal
```

`-Dx6.5c/-2c+w13c/0.5c+h+jTC+ml` : 以圖的頂部中心點(jTC)而言，在6.5公分、-2公分的地方繪製一個13x0.5公分(w13c/0.5c)的橫的(h, horizontal)色階表，數字在上面標在上面(+ml)

- `-D+mc`: 數字和標都在下面
- `-D+ma`: 數字在上面、標在下面
- `-D+ml`: 數字在下面、標在上面

`-By”l”mgal` : y的label(l)是”minigal”

`-Bxa10` : x軸上每距離10就一個數字

## 資料平滑化

```shell
gmt blockmean gravity.xyg -I0.2m -R121.3/121.8/25.0/25.4 > gravity.bm
gmt surface gravity.xyg -Ggravity.grd -I0.5m -R121.3/121.8/25.0/25.4 -T0 -Vn
gmt grdinfo gravity.grd
gmt grdimage gravity.grd -C
```

`blockmean`：資料格點化

- R: 處理的data範圍
- I: 間隔(increment)

`surface`：資料的平滑化

grd檔 : 用二進位的方式在處理資料，有效壓縮檔案

- G : 輸出成網格檔
- R : 想處理的data範圍
- I : 間隔(increment)
- T : Tension factor，範圍在0-1，數字越接近1越接近原始資料

`grdinfo` : 查看grd資料

`grdimage` : 顯示網格資料的型式

- C : 使用的色階

## 繪製陰影圖

```shell
gmt begin plot_NorthTW_Mapview pdf A+m0.5c -C
	gmt makecpt -Cgeo -T-200/1100/100 -Z
	gmt colorbar -Dx5c/-2c+w10c/0.5c+h+jTC+ml -Bxa200f+l"Height" -By+lm
	gmt grdgradient NTW_topo01s.grd -A140/45 -Gshade.nc -Ne0.6
	gmt grdimage NTW_topo01s.grd -R121.3/121.8/25.0/25.4 -Ba0.1f0.1 -B+t"Relief" -JM10c -Ishade.nc
gmt end
pause
```

`grdgradient`：繪製陰影圖的主要指令

- A : 光源的方位角
- G : 輸出的檔名
- N : 振幅計算方法 (e/t) (0~1)

## 透過GMT Server拿檔案

```shell
grdcut @earth_relief_0.1m -R121.3/121.8/25.0/25.4 -Gclasstopo.grd
```
