# Assignment #04

## 範例選用：example 02

```shell
REM		GMT EXAMPLE 02
REM
REM Purpose:	Make two color images based gridded data
REM GMT modules:	set, grd2cpt, grdimage, makecpt, colorbar, subplot
REM

gmt begin ex02
	gmt set MAP_ANNOT_OBLIQUE separate
	gmt subplot begin 2x1 -A+JTL -Fs16c/9c -M0 -R160/20/220/30+r -JOc190/25.5/292/69/16c -B10 -T"H@#awaiian@# T@#opo and @#G@#eoid@#"
		gmt subplot set 0,0 -Ce3c
		gmt grd2cpt @HI_topo_02.nc -Crelief -Z
		gmt grdimage @HI_topo_02.nc -I+a0
		gmt colorbar -DJRM+o1.5c/0+mc -I0.3 -Bx2+lTOPO -By+lkm

		gmt subplot set 1,0 -Ce3c
		gmt makecpt -Crainbow -T-2/14/2
		gmt grdimage @HI_geoid_02.nc
		gmt colorbar -DJRM+o1.5c/0+e+mc -Bx2+lGEOID -By+lm
	gmt subplot end
gmt end show
```

![Untitled](Assignment%20#04%20d7390f61c91343e5964e9ba68112ab8c/Untitled.png)

## 筆記

### 設定圖幅與範圍

```python
gmt set MAP_ANNOT_OBLIQUE separate
```

設定”MAP_ANNOT_OBLIQUE”這張地圖分開

```python
gmt subplot begin 2x1 -A+JTL -Fs16c/9c -M0 -R160/20/220/30+r -JOc190/25.5/292/69/16c -B10 -T"H@#awaiian@# T@#opo and @#G@#eoid@#"
```

subplot：開一張2x1的子地圖

- `-A+JTL` : 設定標記要放在頂端左邊
- `-Fs16c/9c` : 設定圖的大小為16公分乘以9公分
- `-M0` : 不留邊
- `-R160/20/220/30+r` : 設定範圍為東經160到西經140度
- `-JOc190/25.5/292/69/16c` : 設定投影的中心為西經170度、北緯25.5度，軸為西經68度、北緯69度，大小為16公分
- `-B10` : 設定圖幅邊框為10公分
- `-T"H@#awaiian@# T@#opo and @#G@#eoid@#”` ：標題為Hawaiian Topo and Geoid
    - `@#`表示大寫

### 第一張（上方）子圖

```python
gmt subplot set 0,0 -Ce3c
```

設在(1,1)位置的圖的東邊清空3公分

```python
gmt grd2cpt @HI_topo_02.nc -Crelief -Z
```

`grd2cpt`：根據grd（網格）資料產生cpt（色階）資料

- @HI_topo_02.nc：輸出之nc檔名稱
- -Crelief：指定其色階顏色為”relief:
- -Z：其色階生成為連續（非離散）的色階

```python
gmt grdimage @HI_topo_02.nc -I+a0
```

`grdimage`：視覺化「@HI_topo_02.nc」網格資料

- -I+a0：產生光照效果，其光的方位角為0度

```python
gmt colorbar -DJRM+o1.5c/0+mc -I0.3 -Bx2+lTOPO -By+lkm
```

`colorbar`：生成色階表

- `-DJRM+o1.5c/0`：將色階表放在右邊中間並在x方向多往東偏移1.5公分
    - +mc：將色階表資料垂直呈現
- `-I0.3`：設定光照強度為0.3
- `-Bx2+lTOPO`：在色階表x方向2公分的地方增加＂TOPO＂的標籤
- `-By+lkm`：在色階表y方向的地方增加＂km＂的標籤

### 第二張（下方）子圖

```python
gmt subplot set 1,0 -Ce3c
```

設在(2,1)位置的圖的東邊清空3公分

```python
gmt makecpt -Crainbow -T-2/14/2
```

`makecpt`：製作色階表

- -Crainbow：使用＂rainbow＂這個預設色階
- -T-2/14/2：設定色階表的最大值為14、最小值為-2，並每2單位一個間隔

```python
gmt grdimage @HI_geoid_02.nc
```

`grdimage`：視覺化「@HI_topo_02.nc」網格資料

```python
gmt colorbar -DJRM+o1.5c/0+e+mc -Bx2+lGEOID -By+lm
```

`colorbar`：生成色階表

- `-DJRM+o1.5c/0`：將色階表放在右邊中間並在x方向多往東偏移1.5公分
    - `+e`：在色階表上下增加三角形
    - `+mc`：將色階表資料垂直呈現
- `-Bx2+lGEOID`：在色階表x方向2公分的地方增加＂GEOID＂的標籤
- `-By+m`：在色階表y方向的地方增加＂m＂的標籤

### 結束

```python
	gmt subplot end
gmt end show
```
